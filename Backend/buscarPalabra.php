<?php
    require("conex.php");
    $con = conexion();
    $palabra = $_GET["palabra"];
    $array = [];
    $qry = "SELECT * FROM palabras WHERE estatus = 'correcta' AND palabra LIKE '{$palabra}'";
    $res = $con->query($qry);
    if($res->num_rows > 0){
        while($datos = $res->fetch_row()){
            $qryP = "SELECT iconoPais FROM pais WHERE id = {$datos[2]}";
            $resP = $con->query($qryP);
            while($datosP = $resP->fetch_row()){
                $array[] = [$datos[0], $datos[1], $datosP[0], $datos[2]];
            }
        }
        echo json_encode($array, JSON_UNESCAPED_UNICODE);
    }else{
        echo "ninguna";
    }
?>
