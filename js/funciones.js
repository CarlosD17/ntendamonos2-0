/**
* funcion desplegarNotificacion
* Cambia los estilos del div
* para mostrar y ocultar la notificacion
*/
function desplegarNotificacion(){
  mensaje.style.top="0";
  setTimeout(function(){
    mensaje.style.top="-30%";
  },3000);
}
/**
* funcion palabra
* @param texto
* Obtiene el texto a mostrar en la notificacion
* lo agrega al elemento notificaciones
* manda llamar a la funcion desplegarNotificacion
*/
function palabra(texto){
    mensaje = document.getElementById("notificaciones");
    mensaje.innerHTML = "<h1>"+texto+"</h1>";
    desplegarNotificacion();
}

/**
* Function onchange Pais
* manda llamar funcion verificarDescripcion
*/
$(document).ready(function(){
  if($("#paisRegistroR").length){
      var con = "";
    $.ajax({
      url: 'Backend/obtenerPaises.php',
      type:'GET',
      crossDomain: true,
      success: function(data){
          data = JSON.parse(data);
          for(pais in data){
              con = con + "<option value='"+data[pais][0]+"'>"+data[pais][1]+"</option>";
          }
          $("#paisRegistroR").html(con);
      }
    });
  }
});

/**
* function registrar
* Mandar por ajax los datos para realizar el registro
* si existe algun error mostrarlos
*/
$(document).ready(function() {
 $("#registroBoton").click(function() {
        var name = $("#name").val();
        var password = $("#password").val();
        var veripassword = $("#verif_password").val();
        var email = $("#email").val();
        var pais = $("#paisRegistroR").val();
        self.palabra("Procesando.."); if(name!=""&&password!=""&&veripassword!=""&&email!=""&&pais!=""){
            if(password == veripassword){
                $.ajax({
                  url: 'Backend/registrarUsuario.php',
                  type: 'POST',
                  data:{name: name, password:password, verif_password:veripassword, email:email, pais_id:pais},
                  success: function(data){
                      data = data.trim();
                      if(data != "Usuario_Repetido"){
                          if(data != "Email_Repetido"){
                              if(data == "Registro_Exitoso"){
                                  sessionStorage.setItem("notificacion", "Confirmar Cuenta Email");
                                  setTimeout(function(){window.location = "index.html";},2000);
                              }
                          }else{
                             self.palabra("Email Ya Registrado"); 
                          }
                         }else{
                             self.palabra("Usuario Ya Registrado");
                         }
                  }
                });
               }else{
                   self.palabra("Password No Coinciden");       
               }
        }else{
            self.palabra("Completar Campos");
        }
	});
});

/**
* Function mostrarPass
* Muestra el cuadro para recuperar password
*/
$(document).ready(function(){
  $("#olvidarPassword").click(function(){
      $("#contSPass").css('visibility','visible');
      $("#contSPass").css('opacity','1');
      $("#contSin").css('visibility','visible');
  });
});

/**
* Function ocultarRecPas
* Oculta el cuadro para recuperar password
*/
function ocultarRecPas(){
  $("#contSPass").css('visibility','hidden');
  $("#contSPass").css('opacity','0');
  $("#contSin").css('visibility','hidden');
}

/**
* Function loginAcceso
* @param usuar
* @param passw
* Obtiene los datos de login y manda una petición ajax
* a la ruta login/autentificar para saber si el usuario existe
*/
$(function(){
    $("#login").click(function(){
        var usuario = $("#usuario").val();
        var password = $("#password").val();
        if(usuario != "" && password != ""){
            $.ajax({
                url: 'Backend/autentificarUsuario.php',
                type: 'POST',
                data:{usuario: usuario, password:password},
                success: function(data){
                if(data == "Acceso_Denegado"){
                    self.palabra("Usuario o Contraseña Incorrectos");
                  }else if(data == "No_Confirmacion"){
                    self.palabra("Confirmar Cuenta");
                  }else if(data != "Error_Acceso"){
                    data = JSON.parse(data);
                      localStorage.setItem('usuario',data[2]);
                      localStorage.setItem('id_usuario',data[0]);
                      localStorage.setItem('fotoPerfil',data[3]);
                      localStorage.setItem('tipoUsuario',data[4]);
                      window.location = "principal.html";
                  }
                }
              });

        }else{
               self.palabra("Completar Campos");
        }
    });
});

/**
 *  Funcion donde se realiza un login automatico
 *  Si no existe la varible en localStorage
 *  Espera a ingresar un user y password
 */
$(document).ready(function(){
  if($("#logAuto").length){
    if(localStorage.getItem('id_usuario')){
        var user = localStorage.getItem('id_usuario');
      $.ajax({
        url: 'Backend/registrarIngresoUsuario.php',
        type: 'POST',
        data:{usuario: user},
        success: function(data){
          if(data == "Acceso"){
            window.location = "principal.html";
          }
        }
      });
    }
  }
});


//__________________________Sección Perfil
/**
* Function cargarPerfil
* Obtiene el nombre del usuario y carga sus datos
* que se almacenaron en la BD
*/
$(document).ready(function(){
  if($("#perfil4").length){
    var nombre = localStorage.getItem('usuario');
    $("#perfil").css('opacity','0');
    $.ajax({
      url: 'Backend/datosPerfil.php',
      type: 'POST',
      data:{nombre:nombre},
      success: function(data){
        data = JSON.parse(data);
        $("#correo").html(data[1]);
        $("#pais").html(data[4][0]);
        if(data[2] >= 0){
          $("#progreso").css('left',"0%");
        }else{
          $("#progreso").css('left', data[2]+"%");
        }
        setTimeout(function(){
          $("#perfil").attr('src','img/imgPerfil/'+data[3]);
          $("#perfil").css('opacity','1');
        },1000);
        $("#bandera").attr('src','img/banderas/'+data[4][1]);
      }
    });
  }
});

/**
* function cambiarImagen
* Activa el input type file
* Permite subir la nueva imagenIndex
*/
function cambiarImagen(){
  document.getElementById('cover').click();
  document.getElementById('guardarImg').style.display = "block";
  document.getElementById('cancelar').style.display = "block";
}

/**
* Function cancelarCambio
* Cancela el cambio de la imagen de perfil
* regresa la imagen de perfil que estab anteriormente
*/
function cancelarCambio(){
  $("#cover").val();
  $("#perfil").css('opacity','0');
  var nombre = localStorage.getItem('usuario');
  $.ajax({
    url: 'Backend/datosPerfil.php',
    type: 'POST',
    data:{nombre: nombre},
    success: function(data){
      data = JSON.parse(data);
      setTimeout(function(){$("#perfil").attr('src','img/imgPerfil/'+data[3]);
      $("#perfil").css('opacity','1');
    },1000);
      $("#guardarImg").css('display','none');
      $("#cancelar").css('display','none');
    }
  });
}

$(function(){
    $("#cover").change(function(){ 
        var val = $("#cover").val();
        var file_type =     val.substr(val.lastIndexOf('.')).toLowerCase();
        if (file_type  === '.jpg') {   
            $("#perfil").css('opacity','0');
            self.readURL(this);
            sessionStorage.setItem("ultimaFoto",$("#perfil").attr("src"));
        }else{
            $("#cover").val("");
        self.palabra("Subir .jpg");
        }
    });
});

/**
* Function cambiar Imagen Front
* Cambia la imagen del usuario solamente en el front
*/
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            setTimeout(function(){$('#perfil').attr('src', e.target.result);
            $("#perfil").css('opacity','1');
          },1000);
        }
        reader.readAsDataURL(input.files[0]);
    }
}

/**
* Function guardarImagen
* Realiza el cambio en la base de datos
* almacena la nueva imagen de perfil en la carpeta img/imgPerfil
*/
$(document).ready(function(){
    $("#guardarImg").click(function(){
      var file = $("#cover").val();
      var formData = new FormData($('#prueba')[0]);
      var nombre = localStorage.getItem('usuario');
      formData.append("nombre", nombre);
      if(file != ""){
        self.palabra("Guardando Cambios");
        $.ajax({
          url: 'Backend/actualizarFoto.php',
         type: 'POST',
          processData : false,
          contentType: false,
          data: formData,
          success: function(data){
            if(data == "Exitoso"){
              $("#cancelar").css('display','none');
              $("#guardarImg").css('display','none');
                localStorage.setItem('fotoPerfil',nombre+".jpg");
              $("#imgPerM").attr('src','img/imgPerfil/'+nombre+".jpg");
            }
          }
        });
      }else{
          self.palabra("Ingresar Una Imagen");
      }
  });
});
/**
* Function recuperarPass
* Comprueba si existe el usuario
* para enviar el email
*/
$(document).ready(function(){
  $("#recuperar").click(function(){
    var user = $("#UserPass").val();
    if(user != ""){
      $.ajax({
        url: 'Backend/recuperarPass.php',
        type:'GET',
        data: {name:user},
        success: function(data){
          if(data.trim() == "Acceso Denegado"){
            self.palabra("No Existe El Usuario");
          }else if(data.trim() == "Correcto"){
            self.ocultarRecPas();
            $("#UserPass").val("");
            self.palabra("Se Te Ha Enviado Un Email");
          }
        }
      });
    }else{
      self.palabra("Ingrese Un Nombre De Usuario");
    }
  });
});

/**
* confirmarPassword
* Obtiene los datos de las nuevas password
* Enviar ajax a la ruta /login/reestablecerPassword
* En el controlador se hacen las autentificaciones necesarias
* si existe un error estos son mostrados por el ajax
* si no redirecciona a /login con un mensaje mandado desde el controlador
*/
$(document).ready(function(){
  $("#confirmarPassword").click(function(){
    var password = $("#password").val();
    var veripassword = $("#verifpassword").val();
    var user = $_GET("user");
    if (password == "" && veripassword =="") {
      self.palabra("Completar Campos");
    } else if (password == veripassword) {
      $.ajax({
        url: 'Backend/cambiarPassword.php',
        type: 'POST',
        data:{password:password, verif_password:veripassword, user:user},
        success: function(data){
          if(data == "Correcto"){
            self.palabra("Contraseña Cambiada");
            setTimeout(function(){
              window.location="index.html";
            },3000);
          }
        }
      });
    } else{
      self.palabra("Contraseñas No Coinciden");
    }
  });
});

function $_GET(param) {
  url = document.URL;
  url = String(url.match(/\?+.+/));
  url = url.replace("?", "");
  url = url.split("&");
  x = 0;
  while (x < url.length)
  {
  p = url[x].split("=");
  if (p[0] == param)
  {
  return decodeURIComponent(p[1]);
  }
  x++;
  }
}

function notiIndex(){
  var notificacion = $_GET("not");
  if (notificacion == "correcto") {
      self.palabra("Autentificado Correctamente");
  } else if (notificacion == "error") {
      self.palabra("Codigo no Valido");
  }

}

$(function(){
    $("#cancelarPassword").click(function(){
        window.location = "index.html";
    });
});
